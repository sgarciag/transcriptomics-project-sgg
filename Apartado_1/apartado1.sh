"""git clone https://gitlab.com/mbioinfoisciii/transcriptomics-project.git
conda create -n rna_seq
conda activate rna_seq
conda env remove -n rna_seq
conda env create -f environment_rna_seq.yaml
conda activate rna_seq
"""
cd /home/vant/Master/OMICS/transcriptomics-project/Apartado1
#working directory : Apartado1

#indexing the reference genome
#hisat2-build --seed 123 -p 2 REF/chr21.fa REF/chr21


#exploring the data
grep "^+$" input/SRR3480371.chr21.fastq  > input/SRR3480371_lines.txt
grep "^+$" input/SRR3480372.chr21.fastq  > input/SRR3480372_lines.txt
grep "^+$" input/SRR3480373.chr21.fastq  > input/SRR3480373_lines.txt
grep "^+$" input/SRR3480374.chr21.fastq  > input/SRR3480374_lines.txt

wc -l input/SRR3480371_lines.txt
wc -l input/SRR3480372_lines.txt
wc -l input/SRR3480373_lines.txt
wc -l input/SRR3480374_lines.txt

#467560 SRR3480371_lines.txt
#648121 SRR3480372_lines.txt
#1068839 SRR3480373_lines.txt
#800203 SRR3480374_lines.txt


head input/SRR3480371.chr21.fastq
#non paired-end


mkdir out
mkdir out/fastqc

# SEQUENCING QC
mkdir out/fastqc
fastqc -o out/fastqc/ input/*.fastq


mkdir out/hisat2


#read alingments
hisat2 --new-summary --summary-file out/hisat2/SRR3480371.hisat2.summary  --rna-strandness R --seed 123 --phred33 -p 2 -k 1 -x input/REF/chr21 -U input/SRR3480371.chr21.fastq -S out/hisat2/SRR3480371.sam

hisat2 --new-summary --summary-file out/hisat2/SRR3480372.hisat2.summary  --rna-strandness R --seed 123 --phred33 -p 2 -k 1 -x input/REF/chr21 -U input/SRR3480372.chr21.fastq -S out/hisat2/SRR3480372.sam

hisat2 --new-summary --summary-file out/hisat2/SRR3480373.hisat2.summary  --rna-strandness R --seed 123 --phred33 -p 2 -k 1 -x input/REF/chr21 -U input/SRR3480373.chr21.fastq -S out/hisat2/SRR3480373.sam

hisat2 --new-summary --summary-file out/hisat2/SRR3480374.hisat2.summary  --rna-strandness R --seed 123 --phred33 -p 2 -k 1 -x input/REF/chr21 -U input/SRR3480374.chr21.fastq -S out/hisat2/SRR3480374.sam



samtools view -bh out/hisat2/SRR3480371.sam > out/hisat2/SRR3480371.bam
samtools view -bh out/hisat2/SRR3480372.sam > out/hisat2/SRR3480372.bam
samtools view -bh out/hisat2/SRR3480373.sam > out/hisat2/SRR3480373.bam
samtools view -bh out/hisat2/SRR3480374.sam > out/hisat2/SRR3480374.bam

samtools sort out/hisat2/SRR3480371.bam -o out/hisat2/SRR3480371.sorted.bam
samtools sort out/hisat2/SRR3480372.bam -o out/hisat2/SRR3480372.sorted.bam
samtools sort out/hisat2/SRR3480373.bam -o out/hisat2/SRR3480373.sorted.bam
samtools sort out/hisat2/SRR3480374.bam -o out/hisat2/SRR3480374.sorted.bam

samtools index out/hisat2/SRR3480371.sorted.bam 
samtools index out/hisat2/SRR3480372.sorted.bam 
samtools index out/hisat2/SRR3480373.sorted.bam 
samtools index out/hisat2/SRR3480374.sorted.bam 


#to reads to counts
mkdir out/htseq
htseq-count --format=bam  --mode=intersection-nonempty --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name out/hisat2/SRR3480371.sorted.bam input/REF/GRCh38.gencode.v38.annotation.for.chr21.gtf> out/htseq/SRR3480371.htseq

htseq-count --format=bam  --mode=intersection-nonempty --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name out/hisat2/SRR3480372.sorted.bam input/REF/GRCh38.gencode.v38.annotation.for.chr21.gtf> out/htseq/SRR3480372.htseq

htseq-count --format=bam  --mode=intersection-nonempty --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name out/hisat2/SRR3480373.sorted.bam input/REF/GRCh38.gencode.v38.annotation.for.chr21.gtf> out/htseq/SRR3480373.htseq

htseq-count --format=bam  --mode=intersection-nonempty --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name out/hisat2/SRR3480374.sorted.bam input/REF/GRCh38.gencode.v38.annotation.for.chr21.gtf> out/htseq/SRR3480374.htseq


#QC of the alignments and counts
cd out
multiqc .

